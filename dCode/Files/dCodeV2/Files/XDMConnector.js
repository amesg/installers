(function ($){
    xcelsius.sdk.declare("com.infosol.infoburst.XDMConnector");
    xcelsius.sdk.require("xcelsius.sdk.rpc.AddOnConnection");

    com.infosol.infoburst.XDMConnector = function(){
        //Must call super contructor() to inherit all the super properties
        com.infosol.infoburst.XDMConnector.superclass.constructor.apply(this, arguments);
        this.__className = "com.infosol.infoburst.XDMConnector";

        //private variables that have public accessor functions
        this._bUseIBConnection = false;
        this._server="";
        this._bUseSSL = false;
        this._bUseMobileAuth = false;
        this._xdcID = null;
        this._xdcPath = "";
        this._keyValue = "";
        this._user = "";
        this._password = "";
        this._currUser = "";
        this._currentKeys = [];
        this._authLookup = [];
        this._metaTag = "";
        this._mappedRanges = [];
        this._linkedKeys = [];
        this._bShowErrorMessage = true;
        this._refreshErrorMessage = "";
        this._connectionName = "";
        this._resultItems = [];
        
        //private variables
        this._initCalls = 0;
        this._NumInitCalls = 3;
        this._GetDataCallBlocked = false;
        this._bNeedSysInfo = true;
        this._gettingSysInfo = false;
        this._bNeedNTUser = true;
        this._bIBAuthMode = true;
        this._bIsEnterpriseServer = true;
        this._bUseIBConnSet = false;
        this._bXDCPathSet = false;
        this._bXDCIDSet = false;
        this._connectionTimeStamp = null;           //used to track changes to credentials
        this._mobileAuthPort = 8552;

        var self = this;
        $(document).on(com.infosol.xcelsius.objects.IBConnData.initEvent,function(){
            if(self._bUseIBConnSet && self._bUseIBConnection){
                self.grabCredentials();
            }
        });

        $(document).on(com.infosol.xcelsius.objects.IBConnData.failInitEvent, function(){
            self.notifyTriggerFailed();
        });
    };

    xcelsius.sdk.extend(com.infosol.infoburst.XDMConnector,xcelsius.sdk.rpc.AddOnConnection);

    /*Get/Set for connectionName*/
    com.infosol.infoburst.XDMConnector.prototype.connectionName = function (value){
        if(arguments.length > 0){
            if(this._connectionName != value){
                this._connectionName = value;
            }
            return this;
        }else{
            return this._connectionName;
        }
    };

    /*Get/Set for server property*/
    com.infosol.infoburst.XDMConnector.prototype.server = function (value){
        //other functions assume that there will always be server:port as the value
        if(arguments.length > 0){
            if(this._server != value){
                this._server = value.toLowerCase();
                if(this._server.substring(0,7) === "http://"){
                    this._server = this._server.replace("http://","");
                }else if(this._server.substring(0,8) === "https://"){
                    this._server = this._server.replace("https://","");
                }
                if(this._server.indexOf(":") === -1){
                    this._server += ":8551";
                }
            }
            return this;
        }else{
            return this._server;
        }
    };

    /*Get/Set for myXDCID property*/
    com.infosol.infoburst.XDMConnector.prototype.myXDCID = function (value) {
        if(arguments.length > 0){
            this._bXDCIDSet = true;
            if(this._xdcID != value){
                this._xdcID = value;
                if(this._bUseIBConnection){
                    this.doGetXDCDetailCalls();
                }else{
                    if(this._bNeedSysInfo){
                        this.getSysInfo();
                    }else{
                        this.doGetXDCDetailCalls();
                        this.getNTUserName();
                    }
                }
            }
            return this;
        }else{
            return this._xdcID;
        }
    };

    /*get/set for myXDCPath property*/
    com.infosol.infoburst.XDMConnector.prototype.myXDCPath = function (value) {
        if(arguments.length > 0){
            if(this._xdcPath != value){
                this._bXDCPathSet = true;
                this._xdcPath = value;

                if(this._bUseIBConnSet){
                    if(this._bUseIBConnection){
                        this.doGetXDCDetailCalls();
                    }else{
                        if(this._bNeedSysInfo){
                            this.getSysInfo();
                        }else{
                            this.doGetXDCDetailCalls();
                            this.getNTUserName();
                        }
                        
                    }
                }
            }
            return this;
        }else{
            return this._xdcPath;
        }
    };

    /*get/set for myKeyValue property*/
    com.infosol.infoburst.XDMConnector.prototype.myKeyValue = function (value) {
        if(arguments.length > 0){
            if(this._keyValue != value){
                this._keyValue = value;
            }
            return this;
        }else{
            return this._keyValue;
        }
    };

    /*get/set for metaTag property*/
    com.infosol.infoburst.XDMConnector.prototype.metaTag = function (value) {
        if(arguments.length > 0){
            if(this._metaTag != value){
                this._metaTag = value;
            }
            return this;
        }else{
            return this._metaTag;
        }
    };

    /*get/set for myUser property*/
    com.infosol.infoburst.XDMConnector.prototype.myUser = function (value) {
        if(arguments.length > 0){
            if(this._bUseIBConnection && this._bUseIBConnSet)
                return this;
            
            if(this._user != value){
                this._user = value;

                if(!this._bNeedSysInfo){
                    this.doGetXDCDetailCalls();
                    this.getNTUserName();
                }
            }
            return this;
        }else{
            return this._user;
        }
    };

    /*Get/Set for currentNTUser property*/
    com.infosol.infoburst.XDMConnector.prototype.currentNTUser = function (value) {
        if(arguments.length > 0){
            return this;        //read only property
        }else{
            return this._currUser;
        }
    };

    /*Get/Set for password property*/
    com.infosol.infoburst.XDMConnector.prototype.password = function (value) {
        if(arguments.length > 0){
            if(this._bUseIBConnection && this._bUseIBConnSet)
                return this;
            
            if(this._password != value){
                this._password = value;

                if(!this._bNeedSysInfo){
                    this.doGetXDCDetailCalls();
                    this.getNTUserName();
                }
            }
            return this;
        }else{
            return this._password;
        }
    };

    /*Get/Set for resultItems property*/
    com.infosol.infoburst.XDMConnector.prototype.resultItems = function (value) {
        if(arguments.length > 0){
            //read only property
            return this;
        }else{
            return this._resultItems;
        }
    };

    /*Get/Set for mappedRanges property*/
    com.infosol.infoburst.XDMConnector.prototype.mappedRanges = function (value) {
        if(arguments.length > 0){
            if(this._mappedRanges != value){
                this._mappedRanges = value;
            }
            return this;
        }else{
            return null;        //can only set this property
        }
    };
	
	
	com.infosol.infoburst.XDMConnector.prototype.linkedKeys = function (value) {
      return null;
    };




    /*Get/Set for currentKeys property*/
    com.infosol.infoburst.XDMConnector.prototype.currentKeys = function (value) {
        if(arguments.length > 0){
            return this;        //read only
        }else{
            return this._currentKeys;
        }
    };

    /*Get/Set for authLookup property*/
    com.infosol.infoburst.XDMConnector.prototype.authLookup = function (value) {
        if(arguments.length > 0){
            return this;        //read only
        }else{
            return this._authLookup;
        }
    };

    /*Get/Set for useSSL property*/
    com.infosol.infoburst.XDMConnector.prototype.useSSL = function (value) {
        if(arguments.length > 0){
            if(this._bUseIBConnection && this._bUseIBConnSet)
                return this;

            if(this._bUseSSL != value){
                this._bUseSSL = value;
            }

            if(this._bNeedSysInfo){
                this.getSysInfo();
            }else{
                this.doGetXDCDetailCalls();
                this.getNTUserName();
            }
            return this;
        }else{
            return this._bUseSSL;
        }
    };

    /*Get/Set for useMobileAuth property */
    com.infosol.infoburst.XDMConnector.prototype.useMobileAuth = function (value) {
        if(arguments.length > 0){
            if(this._bUseIBConnection && this._bUseIBConnSet)
                return this;

            if(this._bUseMobileAuth != value){
                this._bUseMobileAuth = value;
            }

            if(this._bNeedSysInfo){
                this.getSysInfo();
            }else{
                this.doGetXDCDetailCalls();
                this.getNTUserName();
            }
            return this;
        }else{
            return this._bUseMobileAuth;
        }
    };


    /*Get/Set for useIBConnection property */
    com.infosol.infoburst.XDMConnector.prototype.useIBConnection = function (value) {
        if(arguments.length > 0){
            this._bUseIBConnSet = true;
            var self = this;
            if(this._bUseIBConnection != value){
                this._bUseIBConnection = value;
            }

            if(this._bUseIBConnection){
                //need to check if the connection manager has already come back with a fault
                if(com.infosol.xcelsius.objects.IBConnData.initFault !== true && com.infosol.xcelsius.objects.IBConnData.initialized){
                    //we have already been initialized
                    //possible we were able to initialize before event listensers were attached in constructors of other connectors
                    //TODO have all connectors register with UID at design time and then have them "phone in" after constructing
                    //  so the connection manager knows when all connectors are listening.
                    this.grabCredentials();
                }
            }else{
                if(this._bNeedSysInfo){
                    this.getSysInfo();
                }else{
                    this.doGetXDCDetailCalls();
                    this.getNTUserName();
                }
            }
            return this;
        }else{
            return this._bUseIBConnection;
        }
    };

    /*Get/Set for showRefreshErrorMessage property*/
    com.infosol.infoburst.XDMConnector.prototype.showRefreshErrorMessage = function (value) {
        if(arguments.length > 0){
            if(this._bShowErrorMessage != value){
                this._bShowErrorMessage = value;
            }
            return this;
        }else{
            return this._bShowErrorMessage;
        }
    };

    /*Get/Set for refreshErrorMessage property*/
    com.infosol.infoburst.XDMConnector.prototype.refreshErrorMessage = function (value) {
        if(arguments.length > 0){
            return this;        //read only property
        }else{
            return this._refreshErrorMessage.replace(/\n/g," ");
        }
    };

    //function used to build up the server ref string for json calls
    com.infosol.infoburst.XDMConnector.prototype.buildServerRef = function (forceRest) {
        if(arguments.length == 0){
            forceRest = false;
        }

        var ref = "";
        var server = this._server;
        
        if(this._bUseSSL){
            ref = "https://";
        }else{
            ref = "http://";
        }

        if(this._bUseMobileAuth && !forceRest){
            var parts = server.split(":");
            server = parts[0] + ":" + this._mobileAuthPort;
        }
        
        return ref + server;
    };

    //will get system info from server
    com.infosol.infoburst.XDMConnector.prototype.getSysInfo = function () {
        var self = this;
        if(this._server === "" || this._gettingSysInfo)
            return;
        this._gettingSysInfo = true;
        var fullURL = this.buildServerRef(true) + "/sysinfo?json=1";
        
        self._bNeedSysInfo = false;
        self._bIBAuthMode = true;
        self._mobileAuthPort = 8551;
        self._initCalls++;
        self.doGetXDCDetailCalls();
        self.getNTUserName();
    };

    //will get system nt user from server
    com.infosol.infoburst.XDMConnector.prototype.getNTUserName = function () {
        return this;
    };

    //function that does the detail calls
    //handles converting path to ID and then get detail for xdc
    com.infosol.infoburst.XDMConnector.prototype.doGetXDCDetailCalls = function () {
        if(this._bXDCIDSet){
            if(this._xdcID === -1){     //-1 is our converted flag to let us know this control has been converted to the new path refs
                this._NumInitCalls = 4;     //four calls because we need to do get/xdcid
                this.getXDCID();
            }else{
                this._NumInitCalls = 3;
                this.getXDCDetails();
            }
        }
    };

    //translate path to id
    com.infosol.infoburst.XDMConnector.prototype.getXDCID = function () {
        if(!this.commReady()){
            return this;
        }
        var self = this;
        var fullURL = this.buildServerRef() + "/infoburst/rest/get/xdcid";
        var dispMsg = this.connectionName() + " received the following error from InfoBurst Enterprise:";
        
        
        
        self._initCalls++;
                //no error
               // self._xdcID = data.Result;
                self._bXDCPathSet = true;
                self.getXDCDetails();
        
    };

    //will get the xdc details for key values etc
    com.infosol.infoburst.XDMConnector.prototype.getXDCDetails = function () {
        if(!this.commReady())
            return this;
        var self = this;
        var dispMsg = this.connectionName() + " received the following error from InfoBurst Enterprise:";
        var fullURL = this.buildServerRef() + "/infoburst/rest/list/xdc";

        
    };

    //test if we have all the necessary pieces to make rest calls
    com.infosol.infoburst.XDMConnector.prototype.commReady = function () {
        if(this._server === null || this._server.length === 0 || ((this._bIBAuthMode || this._bUseMobileAuth) && (this._user === null || this._user.length === 0 || this._password === null || this._password.length === 0))){
            return false;
        }
        return true;
    };

    //get xdc data from server
    com.infosol.infoburst.XDMConnector.prototype.getDataFromServer = function () {
        console.log('data');
        if(this._initCalls < this._NumInitCalls){
            this._GetDataCallBlocked = true;
            //return;
        }
        //can we perform the data call
        if(this._xdcID === null || this._keyValue === "" || !this.commReady) {
            //return;
        }

        var self = this;
        var dispMsg = this.connectionName() + " received the following error from InfoBurst Enterprise:";
        var fullURL = this.buildServerRef() + "/infoburst/rest/get/xdc";
        
        //build up request data
        var myData = "json=1&ibec=1&id=" + this._xdcID;

        if(this._keyValue !== null && this._keyValue.length > 0 && this._keyValue !== "_keyValue"){
            myData += "&k=" + encodeURIComponent(this._keyValue);
        }
        if(this._metaTag !== null && this._metaTag.length > 0){
            myData += "&meta=" + encodeURIComponent(this._metaTag);
        }
        var getTheseBlocks = "";
        for (var i = 0; i < this._mappedRanges.length; i++) {
            getTheseBlocks += encodeURIComponent(this._mappedRanges[i]) + ",";
        }
        console.warn(getTheseBlocks);
        if (getTheseBlocks.length > 0) {
            getTheseBlocks = getTheseBlocks.substring(0,getTheseBlocks.length - 1);
            myData += "&v=" + encodeURIComponent(getTheseBlocks);
        }else{
            var msgPopup = new com.infosol.xcelsius.controls.MessagePopup();
            var noBlocksMsg = this.connectionName() + " does not have any ";
            noBlocksMsg += "data sources";
            noBlocksMsg += " mapped to the excel sheet.";
            if(this._bShowErrorMessage){
                msgPopup.show(noBlocksMsg + "\n\nFor optimal performance map a range, or remove the connector.","Cache Connector Information");
            }
            this._refreshErrorMessage = noBlocksMsg + "  For optimal performance map a range, or remove the connector.";
            self.notifyPropertyChanged("refreshErrorMessage", self.refreshErrorMessage, "");
            self.notifyTriggerFailed();
            return;
        }
    
        if(typeof this._busy !== 'undefined')
            this._busy();
        $.ajax({
            // url: fullURL,
            url: "data.txt",
            type: "POST",
            dataType: "json",
            data: myData,
            beforeSend: function(xHdr){
                xHdr.setRequestHeader("Cache-Control","no-cache, no-store, must-revalidate");
                if(self._bIBAuthMode || self._bUseMobileAuth){
                    xHdr.setRequestHeader("Authorization","Basic " + btoa(self._user + ":" + self._password));
                }
            }
        }).done(function (data, textStatus, xHdr) {
            console.log(data);
            if(data.ErrorMessage === undefined){
                //no error
                var orig_resultItems = self._resultItems;

                self._resultItems = [];
                if(data.length > 0){
                    self._resultItems = data;
                }
                self.notifyPropertyChanged("resultItems", self.resultItems, orig_resultItems);
                self.notifyTriggerComplete();
                if(typeof self._idle !== 'undefined')
                    self._idle();
            }else{
                if(data.ErrorMessage.indexOf("Failed to read Object with ID") !== -1){
                    data.ErrorMessage = "XDC no longer on the system.";
                }
                else if(data.ErrorMessage.indexOf("Request Failed :: Not authorized") !== -1){
                    dispMsg += "\n\nYou are not authorized to view this cache.  Please contact your InfoBurst administrator about gaining access to the cache.";
                }else if(data.ErrorMessage.indexOf("Request Failed :: Insufficient access rights") !== -1){
                    dispMsg += "\n\nYou do not have the permission to view this cache.  Please contact your InfoBurst administrator about gaining access to the cache.";
                }else{
                    dispMsg += "\n\n" + data.ErrorMessage;
                }
                if(self._bShowErrorMessage){
                    var msgDialog = new com.infosol.xcelsius.controls.MessagePopup();
                    msgDialog.show(dispMsg,"Cache Connector Error");
                }
                self._refreshErrorMessage = dispMsg;
                self.notifyPropertyChanged("refreshErrorMessage", self.refreshErrorMessage, "");
                self.notifyTriggerFailed();
                if(typeof self._idle !== 'undefined')
                    self._idle();
            }
        }).fail(function (xHdr, textStatus, errorThrown) {
            var msgDialog = new com.infosol.xcelsius.controls.MessagePopup();
            var msg = "Error while getting cache from XDC.";
            msg += com.infosol.xcelsius.objects.ConnectorHelper.handleHttpStatus(xHdr.status, textStatus);
            if(self._bShowErrorMessage){
                msgDialog.show(msg,"Cache Connector Error");
            }
            self._refreshErrorMessage = msg;
            self.notifyPropertyChanged("refreshErrorMessage", self.refreshErrorMessage, "");


            self.notifyTriggerFailed();
            if(typeof self._idle !== 'undefined')
                self._idle();

        });
    };

    com.infosol.infoburst.XDMConnector.prototype.trigger = function () {
        com.infosol.infoburst.XDMConnector.superclass.trigger.apply(this, arguments);
        if(com.infosol.xcelsius.objects.IBConnData.initFault !== true){
            if(this._bUseIBConnection && com.infosol.xcelsius.objects.IBConnData.promptUser && this._initCalls == 2){
                this._user = com.infosol.xcelsius.objects.IBConnData.user;
                this._password = com.infosol.xcelsius.objects.IBConnData.password;
                this._GetDataCallBlocked = true;
                this.doGetXDCDetailCalls();
            }else{
                this.getDataFromServer();
            }
        }else{
            var msgDialog = new com.infosol.xcelsius.controls.MessagePopup();
            var msg = "Error while getting cache from XDC.";
            msg += "  Connection Manager failed to initialize.  Refresh the dashboard to try again.";
            if(this._bShowErrorMessage){
                msgDialog.show(msg,"Cache Connector Error");
            }
            this._refreshErrorMessage = msg;
            this.notifyPropertyChanged("refreshErrorMessage", this.refreshErrorMessage, "");


            this.notifyTriggerFailed();
        }
    };

    com.infosol.infoburst.XDMConnector.prototype.grabCredentials = function () {
        this._bNeedSysInfo = false;
        var msg = "";
        //only perform this if there are truely new credentials to be handled
        if(this._connectionTimeStamp != com.infosol.xcelsius.objects.IBConnData.connectedAt){
            //use public setter so we can validate presence of http vs https and port number
            this.server(com.infosol.xcelsius.objects.IBConnData.server);
            this._bIBAuthMode = com.infosol.xcelsius.objects.IBConnData.ibAuthMode;
            this._user = com.infosol.xcelsius.objects.IBConnData.username;
            this._password = com.infosol.xcelsius.objects.IBConnData.password;
            this._bUseSSL = com.infosol.xcelsius.objects.IBConnData.useSSL;
            this._bUseMobileAuth = com.infosol.xcelsius.objects.IBConnData.useMobileAuth;
            this._mobileAuthPort = com.infosol.xcelsius.objects.IBConnData.mobileAuthPort;
            this._connectionTimeStamp = com.infosol.xcelsius.objects.IBConnData.connectedAt;
            this._initCalls = 2;        //we skipped two calls because we're using the ib connection component
            //no prompt then grab details now
            if(!com.infosol.xcelsius.objects.IBConnData.promptUser){
                this.doGetXDCDetailCalls();
            }
        }
    };
})(jQuery);