using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using Microsoft.Win32;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Net;

namespace MyFirstCustomAction
{
    public class CustomActions
    {
        private const int EXPIREDURATIONDAYS = 5;

        [CustomAction]
        public static ActionResult GenerateTrialKeyFile(Session session)
        {
            session.Log("Begin GenerateTrialKeyFile Custom Action");

            try
            {
                // get path to xcelsius 
                session.Log("get xcelsiusPath");
                string xcelsiusPath = getXcelsiusPath(session);

                // get productId
                session.Log("get productId");
                string productId = session.CustomActionData["ProductIdKey"];

                if (productId.Length != 15)
                {
                    throw new Exception("Error: invalid ProductId");
                }

                // get serialCode
                session.Log("get serialCode");
                string serialCode = session.CustomActionData["SerialCode"];

                serialCode = serialCode.Replace("-", "");
                if (serialCode.Length != 16)
                {
                    throw new Exception("Error: invalid Serial Number");
                }

                // get UniqueIdentifier
                session.Log("get uniqueIdentifier");
                string uniqueIdentifier = getUniqueIdentifier(session);

                if (uniqueIdentifier.Length != 16)
                {
                    throw new Exception("Error: invalid UID");
                }

                // get today date and add 5 days for trial expire.
                session.Log("get expire date");
                DateTime expireDate = DateTime.Today;
                string year = getYearValue(expireDate);
                string month = getMonthValue(expireDate);
                string day = getDayValue(expireDate);

                // set the return code for activation
                string sendCode = "101";

                // Generate the string code 
                session.Log("generate a message");

                // Encode the date and message
                year = caesarCipher(year);
                month = caesarCipher(month);
                day = caesarCipher(day);

                // build the message 
                string message = productId + year + serialCode + month + uniqueIdentifier + day + sendCode;
                message = masCipher(message);

               
                Boolean makeTrialVersion = false;

                // Validate the message via the REST calls, if fails write with temp trial date
                try
                {
                    session.Log("start online validation..");
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://license.infosol.online/api/v1/serial/activate");
                    request.Method = "POST";
                    request.Headers.Add("Authorization", "Basic VE9LRU46MzI3VmN9WTUzMUIyd01U");
                    request.ContentType = "text/html";
                    request.ContentLength = message.Length;
                    request.Expect = "text/html";

                    byte[] requestMessage = Encoding.ASCII.GetBytes(message);
                    request.GetRequestStream().Write(requestMessage, 0, message.Length);

                    session.Log("get http response..");
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    session.Log("check response status..");
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        session.Log("Response is OKAY");
                        // the call was successful, use the returned code within the string to determine what needs doing

                        // Get the stream associated with the response.
                        Stream receiveStream = response.GetResponseStream();

                        // Pipes the stream to a higher level stream reader with the required encoding format. 
                        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        string returnStr = readStream.ReadToEnd();

                        session.Log("returnStr: " + returnStr);

                        // make sure the returned string is the correct size
                        session.Log("check length of return string.. " + returnStr.Length);
                        if (returnStr.Length != 58)
                        {
                            session.Log("return string is not the correct length, fail installation!");

                            throw new Exception("Error: There was a problem with the server response, please contact you account manager.");
                        }
                        else
                        {
                            session.Log("Decipher the return string..");
                            returnStr = masDeCipher(returnStr);

                            session.Log("return string correct length");
                            string returnCode = returnStr.Substring(55, 3);

                            session.Log("check the return code: " + returnCode);
                            if (returnCode == "201")
                            {
                                session.Log("return code is good");

                                // returned license is okay
                                message = returnStr;

                                session.Log(message);

                                // replace the sendCode to 102 for the license file.

                                message = message.Substring(0, 55) + "102";
                                message = masCipher(message);
                                session.Log(message);
                            }
                            else
                            {
                                session.Log("return code is bad, throw exception");

                                throw new Exception("Error " + returnCode + ": There was an problem validating this product, please contact you account manager.");
                            }
                        }

                        // clean up 
                        response.Close();
                        readStream.Close();
                    }
                    else
                    {
                        session.Log("Response is not OKAY, throw webexception");

                        throw new WebException();
                    }

                }
                catch (WebException ex)
                {
                    // the connection has failed, continue with a short expire date
                    session.Log("Internet Response failed, continue with a trial to check later");

                    MessageBox.Show("The installer failed to connect to the internet to validate the serial number. Please use the license manager to validate your product.", "Internet Validation Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    makeTrialVersion = true;
                }
                
                if (makeTrialVersion)
                {
                    session.Log("Make trial version key");

                    // Internet Failed Create a temp key 
                    //expireDate = expireDate.AddDays(EXPIREDURATIONDAYS);
                    expireDate = expireDate.AddDays(-1);
                    year = caesarCipher(getYearValue(expireDate));
                    month = caesarCipher(getMonthValue(expireDate));
                    day = caesarCipher(getDayValue(expireDate));
                    sendCode = "102";

                    message = productId + year + serialCode + month + uniqueIdentifier + day + sendCode;
                    message = masCipher(message);
                }

                // write key to file
                if (System.IO.Directory.Exists(xcelsiusPath))
                {
                    session.Log("xcelsius path exists, generating license...");

                    System.IO.File.WriteAllText(xcelsiusPath + "\\" + productId + ".swf", message);
                }
                else
                {
                    throw new Exception("Xcelsius not found on machine, unable to generate trial license key");
                }
            }
            catch (Exception ex)
            {
                session.Log("Exception occurred as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);

                // Display an error message 
                MessageBox.Show(ex.Message, "Failed Installation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return ActionResult.Failure;
            }

            session.Log("End GenerateTrialKeyFile Custom Action");

            return ActionResult.Success;
        }

        private static string getUniqueIdentifier(Session session)
        {
            session.Log("Begin getUniqueIdentifier function");

            session.Log("retrieve uniqueIdentifier");
            string uniqueIdentifier = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\InfoSol\\UKLabs", "UniqueIdentifier", "");

            // if no present generate one
            if (uniqueIdentifier == "")
            {
                session.Log("generate a new uniqueIdentifier");

                Random randObj = new Random((int)DateTime.Now.Ticks);

                for (int i = 0; i < 16; i++)
                {
                    uniqueIdentifier = uniqueIdentifier.Insert(i, randObj.Next(0, 9).ToString());
                }

                session.Log("store uniqueIdentifier for later use");
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\InfoSol\\UKLabs", "UniqueIdentifier", uniqueIdentifier);
            }

            session.Log("uniqueIdentifier: " + uniqueIdentifier);

            return uniqueIdentifier;
        }

        private static string getXcelsiusPath(Session session)
        {
            session.Log("begin getXcelsiusPath function");

            // Get for propertypanel location registry 
            string propLoc = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\InfoSol\\UKLabs", "PropLoc", "");
            session.Log("propLoc = " + propLoc);

            // if registry exists check folder exists
            if (System.IO.Directory.Exists(propLoc))
            {
                session.Log("prop folder found and exists!");
                return propLoc;
            }
            else
            {
                session.Log("prop folder not found or doesnt exist!");

                // check for xcelsius location registry
                string xcelsiusLoc = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Xcelsius.exe", "", "");
                session.Log("xcelsiusLoc = " + xcelsiusLoc);

                // check if we think its not potentially valid
                if (xcelsiusLoc.IndexOf("Xcelsius 4.0") == -1)
                {
                    session.Log("xcelsius folder not found or doesnt exist!");

                    // Display a dialog box to explain why the open dialog is required
                    DialogResult result = MessageBox.Show("The installer could not locate your xcelsius install, please click OK and browse to your xcelsius install location manually.", "Locate Xcelsius Installation...", MessageBoxButtons.OKCancel);

                    if (result == DialogResult.OK)
                    {

                        // last effort prompt user for location
                        Thread thread = new Thread(() =>
                        {
                            OpenFileDialog openFileDialog1 = new OpenFileDialog();
                            openFileDialog1.Title = "Locate Xcelsius Installation...";
                            openFileDialog1.InitialDirectory = "c:\\program files (x86)\\";
                            openFileDialog1.Filter = "xcelsius (xcelsius.exe)|xcelsius.exe";
                            openFileDialog1.RestoreDirectory = true;
                            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                xcelsiusLoc = openFileDialog1.FileName;
                            }
                        });
                        thread.SetApartmentState(ApartmentState.STA);
                        thread.Start();
                        thread.Join();
                    }

                    session.Log("xcelsiusLoc = " + xcelsiusLoc);
                }

                // check if the xcelsius location exists and set the registry
                if (System.IO.File.Exists(xcelsiusLoc))
                {
                    session.Log("xcelsius folder found and exists!");

                    propLoc = xcelsiusLoc.Replace("Xcelsius.exe", "");
                    propLoc += "assets\\propertySheets";

                    session.Log("propLoc = " + propLoc);

                    if (System.IO.Directory.Exists(propLoc))
                    {
                        session.Log("prop folder found and exists!");

                        // Set the propertypanel location registry
                        Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\InfoSol\\UKLabs", "PropLoc", propLoc);

                        return propLoc;
                    }
                    else
                    {
                        session.Log("xcelsius propertySheets folder does not exist.");  
                    }
                }
                else
                {
                    session.Log("xcelsius folder not found or doesnt exist!");
                }
            }

            return "PathNotFound";
        }

        private static string caesarCipher(string input)
        {
            string from = "0123456789";
            string to   = "ABCDEFGHIJ";

            StringBuilder sb = new StringBuilder(input);

            for (int i = 0; i < input.Length; i++)
            {
                int index = from.IndexOf(input[i]);
                sb[i] = to[index];
            }

            return sb.ToString();
        }

        private static string caesarDeCipher(string input)
        {
            string from = "ABCDEFGHIJ";
            string to = "0123456789";

            StringBuilder sb = new StringBuilder(input);

            for (int i = 0; i < input.Length; i++)
            {
                int index = from.IndexOf(input[i]);
                sb[i] = to[index];
            }

            return sb.ToString();
        }

        private static string masCipher(string input)
        {
            string from = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string to   = "50PLQO8UR6EKZ4Y9DFX2H3WTMCSVGN7J1AIB";

            StringBuilder sb = new StringBuilder(input);

            for (int i = 0; i < input.Length; i++)
            {
                int index = from.IndexOf(input[i]);
                sb[i] = to[index];
            }

            return sb.ToString();
        }

        private static string masDeCipher(string input)
        {
            string from = "50PLQO8UR6EKZ4Y9DFX2H3WTMCSVGN7J1AIB";
            string to   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            StringBuilder sb = new StringBuilder(input);

            for (int i = 0; i < input.Length; i++)
            {
                int index = from.IndexOf(input[i]);
                sb[i] = to[index];
            }

            return sb.ToString();
        }

        private static string getYearValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Year.ToString();
            }

            return result;
        }

        private static string getMonthValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Month.ToString();
                result = result.PadLeft(2);
                result = result.Replace(' ', '0');
            }

            return result;
        }

        private static string getDayValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Day.ToString();
                result = result.PadLeft(2);
                result = result.Replace(' ', '0');
            }

            return result;
        }

        /* immedite customaction does not have priviliges, but also doesn't have access to session...
        [CustomAction]
        public static ActionResult GenerateTrialKeyFile(Session session)
        {
            try
            {
                session.Log("Begin GenerateTrialKeyFile Custom Action");

                // get today date and add 5 days for trial expire.
                DateTime expireDate = DateTime.Today;
                expireDate.AddDays(5);

                // get each element of the key
                string productID = @"000000077659311"; //  session["PIDKEY"].ToUpper();
                string year = CustomActions.getYearValue(expireDate);
                string serialCode = @"1234ABCD5678EFGH";
                string month = getMonthValue(expireDate);
                string machineID = CustomActions.getMachineId();
                string day = CustomActions.getDayValue(expireDate);
                string returnCode = @"101";

                // Generate the string code 
                year = caesarCipher(year);
                month = caesarCipher(month);
                day = caesarCipher(day);

                string message = productID + year + serialCode + month + machineID + day + returnCode;
                message = masCipher(message);

                // Create file in xcelsius 
                string xcelsiusPath = getXcelsiusPath(); // session);

                session.Log("xcelsiusPath = " + xcelsiusPath);

                if (System.IO.Directory.Exists(xcelsiusPath))
                {
                    session.Log("xcelsius path exists, write file...");

                    System.IO.File.WriteAllText(xcelsiusPath + "\\" + productID + ".swf", message);
                }
                else
                {
                    throw new Exception("Xcelsius not found on machine, unable to generate trial license key");
                }

                session.Log("End GenerateTrialKeyFile Custom Action");
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action GenerateTrialKeyFile {0}", ex.ToString());
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }


        private static string getYearValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Year.ToString();
            }

            return result;
        }

        private static string getMonthValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Month.ToString();
                result.PadLeft(2);
                result.Replace(' ', '0');
            }

            return result;
        }

        private static string getDayValue(DateTime date)
        {
            string result = "00";

            if (date != null)
            {
                result = date.Day.ToString();
                result.PadLeft(2);
                result.Replace(' ', '0');
            }

            return result;
        }

        private static string getMachineId()
        {
            string result = "0000000000000000";

            // Check registry for existing MachineID
            const string userRoot = "HKEY_LOCAL_MACHINE";
            const string subkey = "SOFTWARE\\InfoSol\\UKLabs";
            const string keyName = userRoot + "\\" + subkey;
            result = (string)Registry.GetValue(keyName, "UniqueIdentifier", "0000000000000000");

            // if no present generate one
            if (result == "0000000000000000" || result.Length != 16)
            {
                Random randObj = new Random((int)DateTime.Now.Ticks);

                for (int i = 0; i < 16; i++)
                {
                    result = result.Insert(i, randObj.Next(0, 9).ToString());
                }

                Registry.SetValue(keyName, "UniqueIdentifier", result);
            }

            return result;
        }



        //private static string getXcelsiusPath(Session session)
        private static string getXcelsiusPath()
        {
            //session.Log("begin getXcelsiusPath function");

            // Get for propertypanel location registry 
            string userRoot = "HKEY_LOCAL_MACHINE";
            string subkey = "SOFTWARE\\InfoSol\\UKLabs";
            string keyName = userRoot + "\\" + subkey;
            string propLoc = (string)Registry.GetValue(keyName, "PropLoc", "");

            //session.Log("propLoc = " + propLoc);

            // if registry exists check folder exists
            if (System.IO.Directory.Exists(propLoc))
            {
                //session.Log("prop folder found and exists!");

                return propLoc;
            }
            else
            {
                //session.Log("prop folder not found or doesnt exist!");

                // check for xcelsius location registry
                userRoot = "HKEY_LOCAL_MACHINE";
                subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Xcelsius.exe";
                keyName = userRoot + "\\" + subkey;
                string xcelsiusLoc = (string)Registry.GetValue(keyName, "", "");

                //session.Log("xcelsiusLoc = " + xcelsiusLoc);

                // check if we think its not potentially valid
                if (xcelsiusLoc.IndexOf("Xcelsius 4.0") == -1)
                {
                    //session.Log("xcelsius folder not found or doesnt exist!");

                    string result = "";

                    // last effort prompt user for location
                    try
                    {
                        //session.Log("Begin OpenFileChooser");
                        //var task = new Thread(() => GetFile(session));
                        var task = new Thread(() => GetFile(ref result));
                        task.SetApartmentState(ApartmentState.STA);
                        task.Start();
                        task.Join();
                        //session.Log("End OpenFileChooser");
                    }
                    catch (Exception ex)
                    {
                        //session.Log("Exception occurred as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);
                        return "";
                    }

                    xcelsiusLoc = result;//session["TMPFILEPATH"];  
                }

                // check if the xcelsius location exists and set the registry
                if (System.IO.File.Exists(xcelsiusLoc))
                {
                    //session.Log("xcelsius folder found and exists!");

                    propLoc = xcelsiusLoc.Replace("Xcelsius.exe", "");
                    propLoc += "assets\\propertySheets";

                    //session.Log("propLoc = " + propLoc);

                    if (System.IO.Directory.Exists(propLoc))
                    {
                        // Set the propertypanel location registry
                        userRoot = "HKEY_LOCAL_MACHINE";
                        subkey = "SOFTWARE\\InfoSol\\UKLabs";
                        keyName = userRoot + "\\" + subkey;
                        Registry.SetValue(keyName, "PropLoc", propLoc);

                        return propLoc;
                    }
                    else
                    {
                        //session.Log("xcelsius propertySheets folder does not exist.");
                        Record record = new Record(1);
                        record.SetString(0, "xcelsius propertySheets folder does not exist.");
                        //session.Message(InstallMessage.Error, record);
                    }
                }
            }
            
            return "";
        }

        private static void GetFile(ref string result) //Session session)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\program files (x86)\\";
            openFileDialog1.Filter = "xcelsius (xcelsius.exe)|xcelsius.exe";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //session["TMPFILEPATH"] = openFileDialog1.FileName;
                result = openFileDialog1.FileName;
            }
        }
        */
    }
}
